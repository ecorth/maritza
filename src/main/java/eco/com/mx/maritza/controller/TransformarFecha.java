package eco.com.mx.maritza.controller;

import java.time.LocalDate;

public class TransformarFecha {

    public LocalDate getFecha(String fecha){
        String fechaEvaluar = fecha.replaceAll("-", "");
        int anio = Integer.parseInt(fechaEvaluar.substring(0, 4));
        int mes = Integer.parseInt(fechaEvaluar.substring(4, 6));
        int dia = Integer.parseInt(fechaEvaluar.substring(6, 8));
        return LocalDate.of(anio, mes, dia);
    }

}
